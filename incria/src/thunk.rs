//! A mapper based on a function from keys to values.
use std::pin::Pin;

use crate::lazy_mapping::{LazyMapper, LazyMappingBackend};

/// A function from keys to values.
///
/// Should be pure, except for use of other mappings. This ensures recomputation is done when needed.
pub trait Thunk<K, V>: Send + 'static {
    fn compute(&self, key: K) -> Pin<Box<dyn std::future::Future<Output = V> + Send + '_>>;
}

#[derive(Debug, Default)]
pub struct ThunkBackend<T> {
    thunk: T,
}

impl<T> ThunkBackend<T> {
    pub fn new(thunk: T) -> Self {
        Self { thunk }
    }

    pub fn thunk(&self) -> &T {
        &self.thunk
    }
}

impl<K, V: 'static, T> LazyMappingBackend<K, V> for ThunkBackend<T>
where
    T: Thunk<K, V>,
{
    async fn is_dirty(&self, _: &K) -> bool {
        false // only dirty if marked dirty by someting else
    }

    fn compute(&self, key: K) -> impl std::future::Future<Output = V> + Send + '_ {
        self.thunk.compute(key)
    }
}

pub type ThunkMapper<K, V, T> = LazyMapper<K, V, ThunkBackend<T>>;
