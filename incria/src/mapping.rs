use std::ops::Deref;

/// A mapper of keys to values, that memoises the results and re-evaluates when necessary.
///
/// This mapping can be:
///   * Storage or access of externally-modifiable values like files or resources shared with other threads (cells)
///   * Computations that are pure, except for their use of cells above (thunks)
///
/// Mappers should be mainly responsible for tracking and re-performing new computations as appropriate, using the API in [`crate::deps`].
pub trait Mapper {
    /// The key or input type
    type Key;

    /// The result
    type Value;

    /// A wrapper around the result.
    /// This allows things like read guards or Arcs to be returned, so long as they dereference to the correct type.
    type Wrapper<'a>: Deref<Target = Self::Value> + 'a
    where
        Self::Value: 'a;

    /// Get a value by the given key, calculating it if necessary.
    async fn get<'a>(&'a self, key: &Self::Key) -> Self::Wrapper<'a>;
}
