
Running `docker images`, we see an image named `challenge` that presumably has our flag.

We have unrestricted access to the docker daemon running as root, so we can simply make ourselves root inside the container and be able to access everything. `docker run -it --rm --user=0 challenge sh`.

`cat /root/FLAG`: `punk_{E1U2R3V59WIUZUJI}`
