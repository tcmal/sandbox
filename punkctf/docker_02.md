
If we follow the same steps as before, then we see we have the `challenge` image again, but that instead of the flag `/root/flag` just contains a hash of it.

Going back to the host, we can look at the build steps for the `challenge` image with `docker image history challenge`. We see one of the build steps involves echoing the flag hash in to the file, so we can see the flag in plaintext: `punk_{V70P92VJALOS5KMM}`.
