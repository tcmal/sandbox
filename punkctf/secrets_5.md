

the interface we're given shows a bunch of leaked secrets at each commit. running `git log --all`, it seems like they are committing then removing each ssh key.

we search for hostname of the system we're on, then get the ssh key and use it to ssh into root@localhost

`punk_{B2J5I5ZJS5XT6E8J}`
