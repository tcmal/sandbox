
Since we have access to change the pipeline, we can simply remove the hash part to print the flag in plaintext.

Jenkins tries to be smart and censor it in our logs, so to get around this we cut out the `punk_` part of the flag by piping it to `cut -c 5-`.
With the prefix added back, we get `punk_{64J846I332MEAGL4}`.
