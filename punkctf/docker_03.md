
Similar to the last one, we only have a hash of the flag in `/root/flag`.
If we look at the build steps with `docker image history --no-trunc challenge`, it is now copying the file, hashing it then removing it.

Docker images consist of many layers in a specific order, where each layer modifies the filesystem in some way. Each build instruction maps to at most one layer. When we add the flag file, a new layer is created with it in it, and even if we remove the flag later, that layer is still part of our image.

To get to it, we save the image as a tar (`docker save challenge > challenge.tar`), then extract it.

Each layer has a folder with a long hash, and a `layer.tar` inside that.
To quickly search through them all, I used this command:`find -iname '*.tar' -exec sh -c 'echo {}; tar -tf {} | grep FLAG' \;` - this prints out the layer hash, followed by all files inside it containing `FLAG`.

We see only one layer has the `FLAG` file, and once we extract it we can read `opt/flag` to get `punk_{53GAEP9LAWODTO0T}`.
