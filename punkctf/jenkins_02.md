
We can no longer edit the pipeline directly, but we can add stuff to `webpack.config.js` which is executed as a normal JS file, so we can access `$FLAG` with `process.env.flag`

The flag is filtered out again, I went probably overkill and copied a ROT13 function to obscure it:

```js
function cipherRot13(str) {
  str = str.toUpperCase();
  return str.replace(/[A-Z]/g, rot13);

  function rot13(correspondance) {
    const charCode = correspondance.charCodeAt();
    //A = 65, Z = 90
    return String.fromCharCode(
            ((charCode + 13) <= 90) ? charCode + 13
                                    : (charCode + 13) % 90 + 64
           );

  }
}

console.log(cipherRot13(process.env.FLAG))

// rest of webpack.config.js
```

Reversing the ROT13, we get `punk_{7KN3O181O6W1A6XS}`.
